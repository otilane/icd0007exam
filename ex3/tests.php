<?php

require_once __DIR__ . '/../vendor/php-test-framework/public-api.php';

const BASE_URL = 'http://localhost:8080/ex3/';

setBaseUrl(BASE_URL);

function defaultPageIsFrom() {
    navigateTo(BASE_URL);

    assertPageContainsRadioWithName('color');
}

function showsConfirmationFromAfterSelection() {
    navigateTo(BASE_URL);

    setRadioFieldValue('color', 'red');

    clickButton('cmd');

    assertPageContainsButtonWithName('confirm');
}

function choosingNoOnConfirmationPageTakesBackToFilledForm() {
    navigateTo(BASE_URL);

    setRadioFieldValue('color', 'red');

    clickButton('cmd');

    clickButton('confirm', 'no');

    assertPageContainsRadioWithName('color');

    assertThat(getFieldValue('color'), is('red'));
}

function choosingYesOnConfirmationPageRedirectsToFinalPage() {
    navigateTo(BASE_URL);

    setRadioFieldValue('color', 'blue');

    clickButton('cmd');

    disableAutomaticRedirects();

    clickButton('confirm', 'yes');

    assertThat(getResponseCode(), isAnyOf(301, 302, 303));
}

function finalPageShowsSelectedColorName() {
    navigateTo(BASE_URL);

    setRadioFieldValue('color', 'blue');
    clickButton('cmd');
    clickButton('confirm', 'yes');

    assertPageContainsText('Sinine');
}

#Helpers

stf\runTests(new stf\PointsReporter([
    2 => 3,
    3 => 9,
    4 => 16,
    5 => 20]));
