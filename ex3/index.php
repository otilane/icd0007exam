<?php

require_once '../vendor/tpl.php';

$translations = ['red' => 'Punane', 'blue' => 'Sinine'];

$data['fileName'] = 'form.html';

print renderTemplate('main.html', $data);
