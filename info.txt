Eksami alguses peaksite käivitama käsu

  java -jar monitor.jar <teie bitbuket-i kasutajatunnus>

  (olles kataloogis, kuhu te projekti kloonisite).

See programm esitab teie töö automaatselt. Soovitatav on kontrollida
(nupp "Show Uploaded Data"), kas see ka nii on.

Küsimused laeb monitor alla (siia projekti faili questions.txt) siis,
kui esimene ekraanitõmmis on üles saadetud.