<?php

require_once __DIR__ . '/../vendor/php-test-framework/public-api.php';

const BASE_URL = 'http://localhost:8080/ex1/';

setBaseUrl(BASE_URL);

function indexToC() {
    navigateTo(BASE_URL);

    clickRelativeLinkWithText("c.html");

    assertCurrentUrl(BASE_URL . "a/b/c/c.html");
}

function cToA() {
    navigateTo(BASE_URL . 'a/b/c/c.html');

    clickRelativeLinkWithText("a.html");

    assertCurrentUrl(BASE_URL . "a/a.html");
}

function aToE() {
    navigateTo(BASE_URL . 'a/a.html');

    clickRelativeLinkWithText("e.html");

    assertCurrentUrl(BASE_URL . "a/b/c/d/e/e.html");
}

function eToF() {
    navigateTo(BASE_URL . '/a/b/c/d/e/e.html');

    clickRelativeLinkWithText("f.html");

    assertCurrentUrl(BASE_URL . "a/b/c/d/e/f/f.html");
}

function shortestSelf() {

    navigateTo(BASE_URL . 'a/a.html');

    $linkText = "shortest self";

    $href = getHrefFromLinkWithText($linkText);

    clickLinkWithText($linkText);

    assertCurrentUrl(BASE_URL . "a/a.html");

    assertThat(strlen($href), is(0),
        sprintf("'%s' is not the shortest link possible", $href));
}

function shortestAIndex() {

    navigateTo(BASE_URL . 'a/a.html');

    $linkText = "shortest a/index.html";

    $href = getHrefFromLinkWithText($linkText);

    clickLinkWithText($linkText);

    assertCurrentUrl(BASE_URL . "a/");

    assertThat(strlen($href), is(1),
        sprintf("'%s' is not the shortest link possible", $href));
}

#Helpers

function clickRelativeLinkWithText($linkText) {
    $href = getHrefFromLinkWithText($linkText);

    if (preg_match("/:/", $href) || preg_match("/^\//", $href)) {
        throw new RuntimeException("$href is not a relative link");
    }

    clickLinkWithText($linkText);
}

stf\runTests(new stf\PointsReporter([
    1 => 1,
    2 => 2,
    3 => 3,
    4 => 4,
    6 => 6]));
