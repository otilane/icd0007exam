<?php

require_once __DIR__ . '/../vendor/php-test-framework/public-api.php';

function categoryObjectCountIsCorrect() {
    require_once 'ex4.php';

    assertThat(count(findCategories()), is(3));
}

function categoryNamesAreCorrect() {
    require_once 'ex4.php';

    $categoryList = findCategories();

    $categoryNames = getCategoryNames($categoryList);

    assertThat($categoryNames, contains(['cat1', 'cat3', 'cat5']));
}

function eachCategoryContainsCorrectProducts() {

    require_once 'ex4.php';

    $categoryList = findCategories();

    $cat1Products = getProductNumbersByCategoryName($categoryList, 'cat1');
    $cat3Products = getProductNumbersByCategoryName($categoryList, 'cat3');
    $cat5Products = getProductNumbersByCategoryName($categoryList, 'cat5');

    assertThat($cat1Products, contains(['p2', 'p4', 'p5']));
    assertThat($cat3Products, contains(['p6', 'p8']));
    assertThat($cat5Products, contains(['p1', 'p3', 'p7']));
}

#Helpers

function getProductNumbersByCategoryName($categoryList, $categoryName) : array {
    foreach ($categoryList as $category) {
        if ($category->name !== $categoryName) {
            continue;
        }

        $productNumbers = array_map(function ($each) {
            return $each->number;
        }, $category->getProducts());

        sort($productNumbers);

        return $productNumbers;
    }

    throw new RuntimeException('did not find category: ' . $categoryName);
}

function getCategoryNames($categoryList) : array {
    $categoryNames = [];
    foreach ($categoryList as $category) {
        $categoryNames[] = $category->name;
    }

    sort($categoryNames);

    return $categoryNames;
}

stf\runTests(new stf\PointsReporter([
    2 => 7,
    3 => 25]));
