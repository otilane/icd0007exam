<?php

class Category {

    public string $name;
    public array $products;

    public function __construct($name) {
        $this->name = $name;
    }

    public function addProduct($product) {
        $this->products[] = $product;
    }

    public function getProducts() : array {
        return $this->products;
    }

}
