<?php

class Product {
    public string $id;
    public string $number;

    public function __construct($id, $number) {
        $this->id = $id;
        $this->number = $number;
    }

}

