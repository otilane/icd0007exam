CREATE TABLE products (id INTEGER PRIMARY KEY,
                       product_number VARCHAR(255),
                       category VARCHAR(255));

INSERT INTO products VALUES (1, 'p1', 'cat5');
INSERT INTO products VALUES (2, 'p2', 'cat1');
INSERT INTO products VALUES (3, 'p3', 'cat5');
INSERT INTO products VALUES (4, 'p4', 'cat1');
INSERT INTO products VALUES (5, 'p5', 'cat1');
INSERT INTO products VALUES (6, 'p6', 'cat3');
INSERT INTO products VALUES (7, 'p7', 'cat5');
INSERT INTO products VALUES (8, 'p8', 'cat3');
